defmodule NervesBbb.LoRa do

  alias NervesBbb.LoRaInterface

  @app :nerves_bbb
  @me __MODULE__


  def module_config() do
    Application.get_env(@app, @me, [])
  end

  def start_link(config \\ []) do
    {:ok, pid} = module_config() 
    |> Keyword.merge(config)
    |> LoRaInterface.start_link(name: @me)

    spawn(fn -> :timer.sleep(1000); initial_connect() end)
    {:ok, pid}
  end
 
  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end

  def initial_connect() do
    appeui = get_appeui()
    if String.length(appeui) == 16 do
      # App EUI already set to attempt to connect
      connect()
    end
  end

  def reset() do
    LoRaInterface.reset(@me)
  end
  
  def connect() do
    LoRaInterface.connect(@me)
  end

  def get_version() do
    LoRaInterface.get_version(@me)
  end

  def get_deveui() do
    LoRaInterface.get_deveui(@me)
  end

  def get_appeui() do
    LoRaInterface.get_appeui(@me)
  end

  def set_appeui(appeui) do
    LoRaInterface.set_appeui(@me, appeui)
  end

  def set_appkey(appkey) do
    LoRaInterface.set_appkey(@me, appkey)
  end

  def save() do
    LoRaInterface.save(@me)
  end

  def send_message(message) do
    LoRaInterface.send_message(@me, message)
  end

  def get_state() do
    LoRaInterface.get_state(@me)
  end

end


