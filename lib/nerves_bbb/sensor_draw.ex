defmodule NervesBbb.SensorDraw do
  use GenServer
  require Logger

  @moduledoc """
    Simple GenServer to write sensor data to OLED
  """

  # sensor_data fetch sleep
  @timedelay  2000
  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def run do
    GenServer.cast({:global, __MODULE__}, :run)
  end

  def get_state(pid) do
    GenServer.call(pid, {:get_state, pid})
  end

  def init([]) do
    sensor_data = NervesBbb.Environment.get_state(Process.whereis(NervesBbb.Environment)).sensor_data
    {:ok, font} = Chisel.Font.load("/fonts/bitocra.bdf")
    :random.seed(:os.timestamp)
    time = :random.uniform
    timer_ref = Process.send_after(self(), :tick, @timedelay)
    sensor_data = %{}
    state = %{time: time, sensor_data: sensor_data, timer: timer_ref, font: font}
    {:ok, state}
  end

  def handle_info(:tick, state) do
    new_state = run(state)  # <------------ ALWAYS HAS TO RUN IT
    timer_ref = Process.send_after(self(), :tick, @timedelay)
    {:noreply, %{new_state | timer: timer_ref}}
  end

  def handle_call({:get_state, _pid}, _from, state) do
    return = Map.take(state, [:time, :sensor_data])
    {:reply, return, state}
  end

  defp run(state) do
    new_time = state.time + :random.uniform/12
    new_sensor_data = NervesBbb.Environment.get_state(Process.whereis(NervesBbb.Environment)).sensor_data

    new_state = %{state | time: new_time, sensor_data: new_sensor_data, font: state.font}

    NervesBbb.Display.clear()
    put_pixel = fn x, y ->
      NervesBbb.Display.put_pixel(x, y)
    end
    # Format temp and humidity so they keep two decimal points so things stay aligned.
    humidity_string = :erlang.float_to_binary(new_sensor_data.humidity, [decimals: 2])
    temperature_string = :erlang.float_to_binary(new_sensor_data.temperature, [decimals: 2])
    gas_string = :erlang.integer_to_binary(new_sensor_data.gas_resistance)
    pressure_string = :erlang.float_to_binary(new_sensor_data.pressure, [decimals: 2])

    Chisel.Renderer.draw_text("     #{humidity_string}|#{gas_string}", 2, 2, state.font, put_pixel)
    Chisel.Renderer.draw_text("     #{temperature_string}|#{pressure_string}", 2, 16, state.font, put_pixel)

    # Display it!
    NervesBbb.Display.display()
    new_state
  end

end
