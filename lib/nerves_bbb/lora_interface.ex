defmodule NervesBbb.LoRaInterface do
  use GenServer

  require Logger

  @moduledoc """
  Interface for the RN2903 LoRaWAN module
  """

  @uart_device "ttyS1"
  @uart_baud 57600
  @reset_pin 26
  @read_timeout 100

  # Commands
  @connect "mac join otaa"

  # Responses
  @response_ok "ok"
  #@response_error_parameter "invalid_param"
  #@response_error_join "not_joined"
  #@response_error_channel "no_free_ch"
  #@response_error_busy "busy"
  #@response_error_paused "mac_paused"
  #@response_error_length "invalid_data_length"
  #@response_silent "silent"

  @response_accepted "accepted"
  @response_denied "denied"

  @type lora_state :: :initial | :disconnected | :connected

  # System commands
  def sleep(pid, length) do
    @response_ok = lora_send(pid, "sys sleep " <> length)
    :ok
  end

  def reset(pid) do
    @response_ok = lora_send(pid, "sys reset")
    :ok
  end

  def connect(pid) do
    @response_ok = lora_send(pid, @connect)
    :ok
  end

  def get_version(pid) do
    lora_send(pid, "sys get ver")
  end

  def get_deveui(pid) do
    lora_send(pid, "mac get deveui")
  end

  def get_appeui(pid) do
    lora_send(pid, "mac get appeui")
  end

  def set_appeui(pid, eui) do
    @response_ok = lora_send(pid, "mac set appeui " <> eui)
    :ok
  end

  def set_appkey(pid, key) do
    @response_ok = lora_send(pid, "mac set appkey " <> key)
    :ok
  end

  def save(pid) do
    @response_ok = lora_send(pid, "mac save")
    :ok
  end

  def send_message(pid, message) do
    response = lora_send(pid, "mac tx uncnf 1 " <> message)
    case response do
      @response_ok -> :ok
      "busy" -> :error
      "invalid_param" -> :error
    end
  end


  def get_state(pid) do
    GenServer.call(pid, {:get_state})
  end


  @doc """
  Send a command to the LoRa device and receive a single response
  """
  def lora_send(pid, command) do
    GenServer.cast(pid, {:flush})
    String.trim(GenServer.call(pid, {:send, command}))
  end

  def start_link(config, opts \\ []) do
    GenServer.start_link(__MODULE__, config, opts)
  end

  def init([]) do
    Logger.debug("Starting LoRaInterface")
    {:ok, reset} = Circuits.GPIO.open(@reset_pin, :output)
    {:ok, uart} = Circuits.UART.start_link()
    :ok = Circuits.UART.open(uart, @uart_device, speed: @uart_baud, active: false)

    # Reset the radio to get it in a nice state
    Circuits.GPIO.write(reset, 0)
    :timer.sleep(100)
    Circuits.GPIO.write(reset, 1)

    state = %{uart: uart, lora_state: :initial}

    {:ok, state}
  end

  def init(_module) do
    init([])
  end

  @doc """
  Handle asynchronous messages from the LoRa module.
  Stuff like confirmation that the network was joined successfully or downstream messages
  """
  def handle_info({:circuits_uart, @uart_device, data}, state) do
    data = String.trim(data)
    Logger.debug("RX: #{data}")

    connection =
      case data do
        @response_accepted ->
          :connected
        @response_denied ->
          handle_call({:send, @connect}, self(), state)
          :disconnected
        _ ->
          state.lora_state
      end


    new_state = %{ state | lora_state: connection}
    {:noreply, new_state}
  end

  def handle_cast({:send, message}, state) do
    message = String.trim(message) <> "\r\n"
    Circuits.UART.write(state.uart, message)
    {:noreply, state}
  end


  def handle_cast({:flush}, state) do
    Circuits.UART.flush(state.uart, :both)
    {:noreply, state}
  end

  @doc """
  Send a message over UART and receive one response.
  Temporarily puts the UART in passive mode to simplify things
  """
  def handle_call({:send, message}, _from, state) do
    Circuits.UART.configure(state.uart, active: false)
    message = String.trim(message) <> "\r\n"
    Circuits.UART.write(state.uart, message)
    {:ok, response} = Circuits.UART.read(state.uart, @read_timeout)
    Circuits.UART.configure(state.uart, active: true)
    {:reply, response, state}
  end

  def handle_call({:get_state}, _from, state) do
    return = Map.take(state, [:lora_state])
    {:reply, return, state}
  end


end
