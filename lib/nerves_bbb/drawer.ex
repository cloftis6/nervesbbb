defmodule NervesBbb.Drawer do

  use GenServer
  require Logger

  def start_link(default) do
    GenServer.start_link(__MODULE__, default)
  end

  def init(args) do
    Logger.debug("Starting drawer")
    spawn(fn -> draw_lines_forever(1) end)

    {:ok, args}
  end


  def draw_lines_forever(line) do
    #Logger.debug("Drawing line #{line}")
    NervesBbb.Display.line(0, line, 128, line)
    NervesBbb.Display.display()

    :timer.sleep(100)

    if line >= 100 do
      NervesBbb.Display.clear()
      draw_lines_forever(0)
    else
      draw_lines_forever(line + 1)
    end
  end

end
