defmodule NervesBbb.Environment do

  use GenServer
  require Logger

  # sensor_data fetch sleep
  @timedelay  5000
  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def run do
    GenServer.cast({:global, __MODULE__}, :run)
  end

  def get_state(pid) do
    GenServer.call(pid, {:get_state, pid})
  end

  def init([]) do
    Logger.debug("Starting Clik Environment board")

    {:ok, bme} = Bme680.start_link([i2c_device_number: 2, i2c_address: 0x77], [name: Bme680])
    :random.seed(:os.timestamp)
    time = :random.uniform
    timer_ref = Process.send_after(self(), :tick, @timedelay)
    sensor_data = Bme680.measure(bme)
    state = %{time: time, sensor_data: sensor_data, timer: timer_ref, bme: bme}
    {:ok, state}
  end

  def handle_info(:tick, state) do
    new_state = run(state)  # <------------ ALWAYS HAS TO RUN IT
    timer_ref = Process.send_after(self(), :tick, @timedelay)
    {:noreply, %{new_state | timer: timer_ref}}
  end

  def handle_call({:get_state, _pid}, _from, state) do
    return = Map.take(state, [:time, :sensor_data])
    {:reply, return, state}
  end

  defp run(state) do
    new_time = state.time + :random.uniform/12
    new_sensor_data = Bme680.measure(state.bme)

    new_state = %{state | time: new_time, sensor_data: new_sensor_data, bme: state.bme}

    # Format temp and humidity so they keep two decimal points so things stay aligned.
    humidity_string = :erlang.float_to_binary(new_sensor_data.humidity, [decimals: 2])
    temperature_string = :erlang.float_to_binary(new_sensor_data.temperature, [decimals: 2])

    if %{lora_state: :connected} == NervesBbb.LoRa.get_state do
      temp_int = trunc(Float.round(new_sensor_data.temperature, 2) * 100)
        |> Integer.to_string(16)
        |> fix_bytes
      hum_int = trunc(Float.round(new_sensor_data.humidity, 2) * 100)
        |> Integer.to_string(16)
        |> fix_bytes
      press_int = trunc(Float.round(new_sensor_data.pressure, 2) * 100) |> Integer.to_string(16)
        |> fix_bytes
      gas_string = Integer.to_string(new_sensor_data.gas_resistance, 16)
        |> fix_bytes
      r = NervesBbb.LoRa.send_message(temp_int <> hum_int <> press_int <> gas_string)
      case r do
        :ok -> Logger.info("Sent payload")
        _ -> Logger.info("Failed to send payload")
      end
    end
    new_state
  end


  defp fix_bytes(bytes_string) do
    cond do
      rem(String.length(bytes_string),2) == 0 -> bytes_string
      rem(String.length(bytes_string),2) != 0 -> "0" <> bytes_string
    end
  end
end
